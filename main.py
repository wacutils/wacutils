import discord
import math
import socket
import sys # For getting python version
import os
 
prefix = '%'
defaultEncryptionAlpha = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ !" # Length is 64
maxCommandLength = 100
maxDigitLength = 15 # For numtobin
 
client = discord.Client()
token = "<INSERT TOKEN HERE>"
 
def binary(n):
	return bin(n)[2:] # Slice off the "0b" part bin leaves behind
 
def bintodec(n):
    return str(int(n,2))
 
def str_is_num(string):
	for char in string:
		if char not in "0123456789":
			return False
	return True
 
def is_valid_ip(ip):
	try:
		socket.inet_aton(ip) # Source: http://www.cs.cmu.edu/afs/cs/academic/class/15213-f00/unpv12e/libfree/inet_aton.c
		return True
	except socket.error:
		return False
 
def ip_up(ip):
	# Ping only once, only allow ipv4, and timeout after 0.5 seconds
	return os.system("ping -c 1 -4 -W 0.5 \"" + ip + "\" > /dev/null")==0
 
def rot_enc(text, key, alpha):
	out=""
	for char in text:
		out += alpha[(alpha.find(char)+key)%len(alpha)]
	return out
 
def xor_enc(text, key, alpha):
	out=""
	for char in text:
		out += alpha[alpha.find(char)^key]
	return out
 
def get_indexes_not_in_str(str1, str2):
	out=[]
	for i in range(len(str1)):
		if str1[i] not in str2:
			out.append(i)
	return out
 
def get_powers_of_two_upto(n):
	out=[]
	i=0
	result=0
	while result < n:
		out.append(result)
		result = 2**i
		i+=1
	return out
 
def get_str_without_indexes(string, indexes):
	if not indexes:
		return string
 
	out=""
	for i in range(len(string)):
		if i not in indexes:
			out+=string[i]
	return out
 
@client.event
async def on_message(message):
	# We do not want the bot to reply to itself
	if message.author == client.user:
		return
 
	content = message.content
	if not content:
		return
 
	if content[0] is not prefix:
		return
 
	# Ignore commands that are too long, these are likely not to be real commands
	if len(content) > maxCommandLength:
		await client.send_message(message.channel, "Length of command exceeds max length of " + str(maxCommandLength) + " (" + str(len(content)) + ")")
		return
 
	args = content.split(' ')
	argc = len(args)
 
	print("DEBUG: Command entered (argc:" + str(argc) + "):" + content)
 
	if content == prefix+"status":
		verInfo = sys.version_info
		versionStr = str(verInfo[0])+"."+str(verInfo[1])+"."+str(verInfo[2]) # format: major.minor.micro
		await client.send_message(message.channel, "Online, running on Python " + versionStr)
 
	elif content == prefix+"help":
                embed=discord.Embed(title="Help", description="The prefix is %", color=0x00ff00)
                embed.add_field(name="help", value="Displays this page", inline=True)
                embed.add_field(name="status", value="Tells you the python version", inline=True)
                embed.add_field(name="dectobin [number]", value="Gives you the binary representation of a base 10 number", inline=True)
                embed.add_field(name="bintodec [binary number]", value="Gives you the decimal representation of a binary number", inline=True)
                embed.add_field(name="ping [IP]", value="pings ip, and gives whether its up or not", inline=True)
                embed.add_field(name="rot [text] [key (number)] [alphabet (DEF for default)]", value="Runs rot on text", inline=True)
                embed.add_field(name="xor [text] [key (number)] [alphabet (DEF for default)]", value="Runs xor on text", inline=True)
                await client.send_message(message.channel, embed=embed)
 
	elif content.startswith(prefix+"ping "):
		if not is_valid_ip(args[1]):
			await client.send_message(message.channel, "Supplied argument is not a valid IP address")
			return
 
		await client.send_message(message.channel, "IP " + args[1] + " is " + ("up" if ip_up(args[1]) else "down"))
 
	elif content.startswith(prefix+"dectobin "):
		if not args[1]:
			return
 
		if len(args[1]) > maxDigitLength:
			await client.send_message(message.channel, "Length of argument exceeds max length of " + str(maxDigitLength) + " (" + str(len(args[1])) + ")")
			return
 
		if not str_is_num(args[1]):
			await client.send_message(message.channel, "Usage: " + prefix + "dectobin [number]")
			return
		await client.send_message(message.channel, "Binary value of " + args[1] + " is " + binary(int(args[1])))
		
	elif content.startswith(prefix+"bintodec "):
                if not args[1]:
                    return
                if len(args[1]) > 30:
                    await client.send_message(message.channel, "Length of argument exceeds max length of 30 (" + str(len(args[1])) + ")")
                    return
                if not str_is_num(args[1]):
                    await client.send_message(message.channel, "Usage: " + prefix + "bintodec [number]")
                    return
                await client.send_message(message.channel, "Decimal value of " + args[1] + " is " + bintodec(args[1]))
                                          
	# xor/rot encryption commands
	if content.startswith(prefix+"rot ") or content.startswith(prefix+"xor "):
		if argc < 3:
			await client.send_message(message.channel, "Usage: " + prefix+content[1:5] + "[text] [key] [alphabet (DEF for default)]")
			return
 
#		alpha = defaultEncryptionAlpha if args[-1] is "DEF" else args[-1]
		alpha=""
		if args[-1] is "DEF":
			alpha = defaultEncryptionAlpha
		else:
			alpha = args[-1]
 
		print("args[-1] = \"" + args[-1] + "\"")
 
		key = args[-2]
		text=""
		for i in range(1,argc-2): # Skips the first command argument
			text += args[i] + ' '
		text = text[0:-1] # Cut off last space
 
		# We don't want any invalid arguments here
		if (not alpha) or (not key) or (not text) or (not str_is_num(key)):
			await client.send_message(message.channel, "Usage: " + prefix+content[1:5] + "[text] [key] [alphabet (DEF for default)]")
			return
 
#		if int(key) > 2**(math.floor(math.log2(len(content))) + 1):
#			await client.send_message(message.channel, "Usage: " + prefix+content[1:5] + "[text] [key] [alphabet (DEF for default)]")
#			return
 
		indexesNotInStr = get_indexes_not_in_str(text, alpha)
		if indexesNotInStr:
			await client.send_message(message.channel, "Warning: Some characters in text are not in the alphabet. Ignoring these characters")
			text = get_str_without_indexes(text, indexesNotInStr) # Remove characters not in alpha
 
		if content[len(prefix)] is "r": # r for rot
			if int(key) > len(alpha):
				await client.send_message(message.channel, "Key is out of range 0 to " + str(len(alpha)))
				return
 
			await client.send_message(message.channel, rot_enc(text, int(key), alpha))
 
		else: # We've already done this check
			nextPowerOfTwo = 2**(math.floor(math.log2(len(alpha))) + 1)
			if int(key) > nextPowerOfTwo:
				await client.send_message(message.channel, "Key is out of range 0 to " + str(nextPowerOfTwo))
				return
 
			if len(alpha) not in get_powers_of_two_upto(len(alpha)): # TODO Minimize this line to use math.log2 to check this
				await client.send_message(message.channel, "Warning: Length of the alphabet is not a power of two, gap will be filled with spaces")
				alpha += ' '*(nextPowerOfTwo - len(alpha)) # Add spaces to fill the gap
 
			await client.send_message(message.channel, xor_enc(text, int(key), alpha))
 
		print("text=" + text + ", key=" + key + ", alpha=" + alpha)
		print("encryped xor:"+xor_enc(text, int(key), alpha))
		print("nextPowerOfTwo:"+str(nextPowerOfTwo))
		print("len(alpha)=" + str(len(alpha)))
 
@client.event
async def on_ready():
	print("Logged in as " + client.user.name)
	print("Client ID: " + client.user.id)
	await client.change_presence(game=discord.Game(name="%help", type=1))
 
client.run(token)